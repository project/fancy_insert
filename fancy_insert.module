<?php


/**
 * Implementation of hook_help().
 */
function fancy_insert_help($path, $arg) {
  switch ($path) {
    case 'admin/help#fancy_insert':
      $output = '<p>'. t('The Fancy Insert module allows users to easily insert images into posts. Each image field can be configured to use Fancy Insert.') .'</p>';
      $output .= '<p>'. t('If !jquery_ui module is enabled, users may drag the fancy insert widget arround the screen.',
      array('!jquery_ui' => l('Jquery UI', 'http://drupal.org/project/jquery_ui', array('absolute' => TRUE)))) .'</p>';
      return $output;
  }
}

/**
 * Implementation of hook_widget_settings_alter().
 */
function fancy_insert_widget_settings_alter(&$settings, $op, $widget) {
  // Only support modules that implement hook_insert_widgets().
  $widget_type = isset($widget['widget_type']) ? $widget['widget_type'] : $widget['type'];
  if (!in_array($widget_type, array('imagefield_widget'))) {
    return;
  }

  // Add our new options to the list of settings to be saved.
  if ($op == 'save') {
    $settings = array_merge($settings, fancy_insert_widget_settings());
  }

  // Add the additional settings to the form.
  if ($op == 'form') {
    $settings = array_merge($settings, fancy_insert_widget_form($widget));
  }
}

/**
 * A list of settings needed by Fancy Insert module on widgets.
 */
function fancy_insert_widget_settings() {
  return array(
    'fancy_insert',
  );
}

/**
 * Configuration form for editing insert settings for a field instance.
 */
function fancy_insert_widget_form($widget) {

  $form['fancy_insert'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fancy insert'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('These options allow the user to easily insert an HTML tags into text areas or WYSIWYG editors after uploading a file or image.'),
    '#weight' => 15,
  );

  $form['fancy_insert']['fancy_insert'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable fancy insert widget'),
    '#default_value' => (bool) $widget['fancy_insert'],
    '#description' => t('Allows users to uncouple the imagefield widget to see images next to the textarea.'),
  );

  return $form;
}

/**
 * Implementation of hook_form_alter().
 */
function fancy_insert_form_alter($form, $form_state, $form_id) {
  if ($form_id == 'content_field_edit_form') {
    if (!empty($form['widget']['fancy_insert'])) {
      $form['#validate'][] = 'fancy_insert_check_config';
      $form['#submit'][] = 'fancy_insert_reload_cache';
    }
  }
  if (isset($form['#node']->type) && ($form['#node']->type . '_node_form' == $form_id)) {
    $widgets = _fancy_insert_load_widgets();
  }
  if (count($widgets[$form['#node']->type])) {
    drupal_add_js(array('fancy_insert' => $widgets[$form['#node']->type]), 'setting');
    drupal_add_css(drupal_get_path('module', 'fancy_insert') . '/fancy_insert.css');
    drupal_add_js(drupal_get_path('module', 'fancy_insert') . '/fancy_insert.js');
    if (module_exists('jquery_ui')) {
      jquery_ui_add('ui.draggable');
    }
  }
}

function fancy_insert_check_config($form, $form_state) {
  if (empty($form_state['values']['insert']) && !empty($form_state['values']['fancy_insert'])) {
    form_error($form['widget']['insert']['insert'], t('Insert must be enabled to use Fancy Insert with this field.'));
  }
}

function fancy_insert_reload_cache() {
  cache_clear_all('fancy_insert', 'cache');
}

/**
 * Function to load all the fancy insert widgets configured per content type.
 */
function _fancy_insert_load_widgets($reset = FALSE) {
  if (!$reset && ($cache = cache_get('fancy_insert')) && !empty($cache->data)) {
    return $cache->data;
  }
  else {
    $fields = content_fields();
    $node_types = array_keys(node_get_types('names'));
    $widgets = array();
    foreach ($node_types as $node_type) {
      foreach ($fields as $field_name => $field) {
        $field = content_fields($field_name, $node_type);
        $widget = $field['widget'];
        if ($widget['type'] == 'imagefield_widget') {
          if ($widget['fancy_insert']) {
            if (!isset($widget[$node_type])) {
              $widgets[$node_type] = array();
            }
            $widgets[$node_type][] = str_replace('_', '-', $field_name) . '-items';
          }
        }
      }
    }
    cache_set('fancy_insert', $widgets, 'cache');
    return $widgets;
  }
}
