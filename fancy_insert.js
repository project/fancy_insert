
Drupal.behaviors.fancy_insert = function(context) {
  $.each(Drupal.settings.fancy_insert, function(key, value) {
    if ($('#' + value + '-fancy_insert-button').length > 0) {
      return false;
    }
    else {
      $("#" + value).prepend('<a href ="javascript:;" id = "' + value + '-fancy_insert-button" class = "fancy_insert-button">' + Drupal.t('Expand') + '</a>');
    }
    $('#' + value + '-fancy_insert-button').bind('click', function (event) {
      if ($(this).text() == Drupal.t('Expand')) {
        $('#' + value).addClass('fancy_insert');
        $('#' + value + ' .dragable td:last').hide();
        $('#' + value + ' .dragable td:first').attr("colspan", 2);
        $('#' + value + ' img').bind('click', function (e) {
          $(this).parents(".widget-preview").next().find(".insert-button").trigger("click");
        });
        $(this).text(Drupal.t('Close'));
        if (jQuery().draggable) {
          $('#' + value).draggable({ refreshPositions: true });
        }
      }
      else {
        $('#' + value).removeClass('fancy_insert');
        $('#' + value + ' .dragable td:last').show();
        $('#' + value + ' .dragable td:first').attr("colspan", 1);
        $('#' + value + ' img').unbind('click');
        $('#' + value).removeAttr('style');
        $(this).text(Drupal.t('Expand'));
      }
    });
  });
}
